SELECT setval('activity_id_seq', coalesce(max(id),0) + 1, false) FROM activity;
SELECT setval('class_id_seq', coalesce(max(id),0) + 1, false) FROM class;
SELECT setval('group_id_seq', coalesce(max(id),0) + 1, false) FROM group;
SELECT setval('institution_id_seq', coalesce(max(id),0) + 1, false) FROM institution;
SELECT setval('message_id_seq', coalesce(max(id),0) + 1, false) FROM message;
SELECT setval('student_class_id_seq', coalesce(max(id),0) + 1, false) FROM student_class;
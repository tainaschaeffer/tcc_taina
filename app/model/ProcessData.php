<?php

class ProcessData extends TRecord
{
    const TABLENAME  = 'process_data';
    const PRIMARYKEY = 'id';
    const IDPOLICY   =  'serial'; // {max, serial}

    private $student;

    /**
     * Constructor method
     */
    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('type');
        parent::addAttribute('description');
        parent::addAttribute('description_id');
        parent::addAttribute('date_process');
        parent::addAttribute('prob_0');
        parent::addAttribute('prob_1');
        parent::addAttribute('student_id');
        parent::addAttribute('group_id');            
    }

    public function set_student(SystemUsers $object)
    {
        $this->student = $object;
        $this->student_id = $object->id;
    }

    public function get_student()
    {
    
        // loads the associated object
        if (empty($this->student))
            $this->student = new SystemUsers($this->student_id);
    
        // returns the associated object
        return $this->student;
    }   

    public function get_student_name()
    {
    
        // loads the associated object
        if (empty($this->student))
            $this->student = new SystemUsers($this->student_id);
    
        // returns the associated object
        return $this->student->name;
    }
}


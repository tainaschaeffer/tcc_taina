CREATE TABLE activity( 
      id  INT  AUTO_INCREMENT    NOT NULL  , 
      name varchar  (45)   NOT NULL  , 
      description text   NOT NULL  , 
      class_id int   NOT NULL  , 
 PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci; 

CREATE TABLE class( 
      id  INT  AUTO_INCREMENT    NOT NULL  , 
      name varchar  (45)   NOT NULL  , 
      institution_id int   NOT NULL  , 
 PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci; 

CREATE TABLE group( 
      id  INT  AUTO_INCREMENT    NOT NULL  , 
      name varchar  (45)   NOT NULL  , 
      class_id int   NOT NULL  , 
 PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci; 

CREATE TABLE institution( 
      id  INT  AUTO_INCREMENT    NOT NULL  , 
      name varchar  (45)   NOT NULL  , 
 PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci; 

CREATE TABLE message( 
      id  INT  AUTO_INCREMENT    NOT NULL  , 
      subject varchar  (100)   NOT NULL  , 
      message text   NOT NULL  , 
      student_class_id int   NOT NULL  , 
      activity_id int   NOT NULL  , 
 PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci; 

CREATE TABLE student_class( 
      id  INT  AUTO_INCREMENT    NOT NULL  , 
      class_id int   NOT NULL  , 
      student_id int   NOT NULL  , 
 PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci; 

 
  
 ALTER TABLE activity ADD CONSTRAINT fk_activity_1 FOREIGN KEY (class_id) references class(id); 
ALTER TABLE class ADD CONSTRAINT fk_class_1 FOREIGN KEY (institution_id) references institution(id); 
ALTER TABLE group ADD CONSTRAINT fk_group_1 FOREIGN KEY (class_id) references class(id); 
ALTER TABLE message ADD CONSTRAINT fk_message_1 FOREIGN KEY (student_class_id) references student_class(id); 
ALTER TABLE message ADD CONSTRAINT fk_message_2 FOREIGN KEY (activity_id) references activity(id); 
ALTER TABLE student_class ADD CONSTRAINT fk_student_class_1 FOREIGN KEY (class_id) references class(id); 

  

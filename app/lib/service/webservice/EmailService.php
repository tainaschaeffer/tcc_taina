<?php
require_once 'app/lib/util/request.php';

/**
 * EmailService
 * Date 18-02-2019
 *
 * @version    1.0
 * @author     Lucas Tomasi, Eduardo Caron
 * @copyright  Copyright (c) 2006-2014 Adianti Solutions Ltd. (http://www.adianti.com.br)
 * @license    http://www.adianti.com.br/framework-license
 */

class EmailService
{
    private $endpoint;
    private $token;

    private $assunto;
    private $mensagem;
    private $responder_para;
    private $de;
    private $para;
    private $copia;
    private $copia_oculta;
    private $anexos;

    public function __construct()
    {
        $ini = parse_ini_file("app/config/mail-api.ini");

        // properties webservice
        $this->endpoint    = $ini['endpoint'];
        $this->token       = $ini['token'];
        $this->system      = $ini['system'];
        $this->autenticado = $ini['autenticado']??NULL;

        $para         = [];
        $copia        = [];
        $copia_oculta = [];
        $anexos       = [];
    }

    public function setAssunto($assunto)
    {
        $this->assunto = $assunto;
    }

    public function getAssunto()
    {
        return $this->assunto;
    }

    public function setMensagem($mensagem)
    {
        $this->mensagem = $mensagem;
    }

    public function getMensagem()
    {
        return $this->mensagem;
    }

    public function setDe($email, $nome = '')
    {
        $this->de = ['email' => $email, 'nome' => $nome];
    }

    public function getDe()
    {
        return $this->de;
    }

    public function setReponderPara($email, $nome = '')
    {
        $this->responder_para = ['email' => $email, 'nome' => $nome];
    }

    public function getReponderPara()
    {
        return $this->responder_para;
    }

    public function setPara($para)
    {
        $this->para = $para;
    }

    public function getPara()
    {
        return $this->para;
    }

    public function addPara($email, $nome = '')
    {
        $this->para[] = ['email' => $email, 'nome' => $nome];
    }

    public function setCopia($copia)
    {
        $this->copia = $copia;
    }

    public function getCopia()
    {
        return $this->copia;
    }

    public function addCopia($email, $nome = '')
    {
        $this->copia[] = ['email' => $email, 'nome' => $nome];
    }

    public function setCopiaOculta($copia_oculta)
    {
        $this->copia_oculta = $copia_oculta;
    }

    public function getCopiaOculta()
    {
        return $this->copia_oculta;
    }

    public function addCopiaOculta($email, $nome = '')
    {
        $this->copia_oculta[] = ['email' => $email, 'nome' => $nome];
    }

    public function setAnexos($anexos)
    {
        $this->anexos = $anexos;
    }

    public function getAnexos()
    {
        return $this->anexos;
    }

    /**
     * Adiciona um anexo ao email
     * @author Eduardo Caron
     * @date   2019-03-22
     * @param  [type]     $anexo    arquivo ou link
     * @param  string     $nome     nome do arquivo
     * @param  string     $tipo     anexos do e-mail [nome, {caminho     | arquivo}]
     *                                                      {url publica | base64}
     */
    public function addAnexo($anexo, $nome = '', $tipo = 'arquivo')
    {
        $this->anexos[] = ['nome' => $nome, $tipo => $anexo];
    }

    public function send()
    {
        $params = [
            'system'         => $this->system,
            'assunto'        => $this->assunto,
            'mensagem'       => base64_encode($this->mensagem),
            'para'           => $this->para,
            'copia'          => $this->copia,
            'copia_oculta'   => $this->copia_oculta,
            'anexos'         => $this->anexos,
            'responder_para' => $this->responder_para,
            'de'             => $this->de
        ];

        if($this->autenticado)
        {
            $params['autenticado'] = $this->autenticado;
        }

        return request($this->endpoint.'send', 'POST', $params, $this->token);
    }
}
?>
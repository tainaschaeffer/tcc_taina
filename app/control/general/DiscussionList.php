<?php

class DiscussionList extends TPage
{
    protected $form;
    private $formFields          = [];
    private static $database     = 'tcc';
    private static $activeRecord = 'Message';
    private static $primaryKey   = 'id';
    private static $formName     = 'list_Message';
    private $showMethods         = ['onReload', 'onSearch'];

    /**
     * Form constructor
     * @param $param Request
     */
    public function __construct( $param )
    {
        parent::__construct();

        // creates the form
        $this->form = new BootstrapFormBuilder(self::$formName);

        $message = new TEntry('message');

        $message->setSize('100%');

        $row        = $this->form->addFields([new TLabel("Mensagem:"), $message]);
        $row->style = ['col-sm-12'];

        //create the form actions
        $btn_onsearch = $this->form->addAction("Buscar", new TAction([$this, 'onSearch']), 'fas:search #ffffff');
        $btn_onsearch->addStyleClass('btn-primary'); 

        $btn_onnew = $this->form->addAction("Perguntar", new TAction(['DiscussionForm', 'onEdit']), 'fas:plus #69aa46');

        // creates a Datagrid
        $this->datagrid        = new TDataGrid;
        $this->datagrid        = new BootstrapDatagridWrapper($this->datagrid);
        $this->filter_criteria = new TCriteria;
        $this->datagrid->disableHtmlConversion();

        $this->datagrid->style = 'width: 100%';
        $this->datagrid->setHeight(320);

        $column_message     = new TDataGridColumn('message', "Mensagem", 'left');
        $column_answers     = new TDataGridColumn('id', "Respostas", 'center');

        $column_answers->setTransformer(['Message', 'getTotalAnswers']);

        $this->datagrid->addColumn($column_message);
        $this->datagrid->addColumn($column_answers);

        $action_onParticipate = new TDataGridAction(array('DiscussionForm', 'onParticipate'));
        $action_onParticipate->setUseButton(false);
        $action_onParticipate->setButtonClass('btn btn-default btn-sm');
        $action_onParticipate->setLabel("Participar");
        $action_onParticipate->setImage('fa:arrow-right #00BCD4');
        $action_onParticipate->setField(self::$primaryKey);
        
        $action_onAnswer = new TDataGridAction(array($this, 'onAnswer'));
        $action_onAnswer->setUseButton(false);
        $action_onAnswer->setButtonClass('btn btn-default btn-sm');
        $action_onAnswer->setLabel("Participar");
        $action_onAnswer->setImage('fa:arrow-right #00BCD4');
        $action_onAnswer->setField(self::$primaryKey);

        $action_onEdit = new TDataGridAction(array('DiscussionForm', 'onEdit'));
        $action_onEdit->setUseButton(false);
        $action_onEdit->setButtonClass('btn btn-default btn-sm');
        $action_onEdit->setLabel("Editar");
        $action_onEdit->setImage('far:edit #478fca');
        $action_onEdit->setField(self::$primaryKey);
        $action_onEdit->setDisplayCondition(array($this, 'displayOption'));

        $action_onView = new TDataGridAction([$this, 'onView'],   ['message_id' => '{id}'] );

        // add the actions to the datagrid
        $this->datagrid->addAction($action_onView, 'Visualizar', 'fa:search blue');
        $this->datagrid->addAction($action_onParticipate);
        $this->datagrid->addAction($action_onEdit);

        // create the datagrid model
        $this->datagrid->createModel();

        // creates the page navigation
        $this->pageNavigation = new TPageNavigation;
        $this->pageNavigation->enableCounters();
        $this->pageNavigation->setAction(new TAction(array($this, 'onReload')));
        $this->pageNavigation->setWidth($this->datagrid->getWidth());

        $panel = new TPanelGroup;
        $panel->add($this->datagrid);
        $panel->addFooter($this->pageNavigation);

        // vertical box container
        $container = new TVBox;
        $container->style = 'width: 100%';
        //$container->add(TBreadCrumb::create(["Geral","Discussões"]));
        $container->add($this->form);
        $container->add($panel);

        parent::add($container);
    }

    public static function onView($param)
    {
        try
        {
            TTransaction::open(self::$database);

            $message_id = $param['id'];
            $message    = new Message($message_id);

            $info  = '<b>Turma:</b><br>'.$message->group->class->name . '<br>';
            $info .= '<b>Grupo:</b><br>'.$message->group->name . '<br>';
            $info .= '<b>Atividade:</b><br>'.$message->activity->name.'<br>';
            $info .= '<b>Autor:</b><br>'.Message::getStudentName($message_id);

            TTransaction::close();
            
            $window = TWindow::create('Dados da mensagem', 0.7, 0.4);
            $window->add($info);
            $window->show();
        }
        catch (Exception $e) // in case of exception
        {
            // shows the exception error message
            new TMessage('error', $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

    public function onAnswer($param)
    {
        
    }

    public function displayOption($object)
    {
        $student_id = SystemUsers::newFromLogin(TSession::getValue('login'))->id;

        if ($object->student_id == $student_id)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function onSearch($param)
    {

    }

    /**
     * Load the datagrid with data
     */
    public function onReload($param = NULL)
    {
        try
        {
            // open a transaction with database 'tcc'
            TTransaction::open(self::$database);

            // creates a repository for Activity
            $repository = new TRepository(self::$activeRecord);
            $limit      = 20;

            $criteria = clone $this->filter_criteria;

            if (empty($param['order']))
            {
                $param['order'] = 'id';    
            }

            if (empty($param['direction']))
            {
                $param['direction'] = 'desc';
            }

            $user_id = ApplicationService::getUserID();
            $groups = ApplicationService::getUserGroupsIDS();
            if(!in_array(1, $groups) || !in_array(3, $groups))
            {
                $criteria->add(new TFilter('group_id', 'IN', "(SELECT group_id FROM student_group WHERE student_id = {$user_id})"));
            }
            elseif(in_array(3, $groups))
            {
                $criteria->add(new TFilter('group_id', 'IN', "(SELECT groups.id FROM groups, class WHERE groups.class_id = class.id and class.teacher_id = {$user_id})"));
            }

            //$criteria->add(new TFilter('group_id', 'IN', "(SELECT group_id FROM student_group WHERE student_id = {$user_id} and student_id IN (SELECT system_user_id FROM system_user_group WHERE system_group_id IN (1, 3)))"));

            $criteria->setProperties($param); // order, offset
            $criteria->setProperty('limit', $limit);

            if($filters = TSession::getValue(__CLASS__.'_filters'))
            {
                foreach ($filters as $filter) 
                {
                    $criteria->add($filter);       
                }
            }

            // load the objects according to criteria
            $objects = $repository->load($criteria, FALSE);
            $this->datagrid->clear();
            if ($objects)
            {
                // iterate the collection of active records
                foreach ($objects as $object)
                {
                    // add the object inside the datagrid
                    $this->datagrid->addItem($object);
                }
            }

            // reset the criteria for record count
            $criteria->resetProperties();
            $count= $repository->count($criteria);

            $this->pageNavigation->setCount($count); // count of records
            $this->pageNavigation->setProperties($param); // order, page
            $this->pageNavigation->setLimit($limit); // limit

            // close the transaction
            TTransaction::close();
            $this->loaded = true;
        }
        catch (Exception $e) // in case of exception
        {
            // shows the exception error message
            new TMessage('error', $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

    public function onShow($param = null)
    {

    }

    /**
     * method show()
     * Shows the page
     */
    public function show()
    {
        // check if the datagrid is already loaded
        if (!$this->loaded AND (!isset($_GET['method']) OR !(in_array($_GET['method'],  $this->showMethods))) )
        {
            if (func_num_args() > 0)
            {
                $this->onReload( func_get_arg(0) );
            }
            else
            {
                $this->onReload();
            }
        }
        parent::show();
    }
}
<?php
class Preference extends TRecord
{
    const TABLENAME  = 'preferencia';
    const PRIMARYKEY = 'id';
    const IDPOLICY   = 'serial'; // {max, serial}

    private $usuario;

    use SystemChangeLogTrait;
    /**
     * Constructor method
     */
    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('user_id');
        parent::addAttribute('fl_filter_hidden');
        parent::addAttribute('fl_menu_hiddenn');
    }
    
    public static function getPreferenceByUser($user_id)
    {
        return Preference::where('user_id', '=', $user_id)->first();
    }
}
?>
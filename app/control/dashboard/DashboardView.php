<?php

class DashboardView extends TPage
{
    private $container;
    private $fieldlist;
    private $filter_criteria;
    private $filter_criteria2;
    private static $formName = 'form_Dashboard';

    private $form;      // search form
    private $datagrid;  // listing
    private $datagrid2;  // listing
    private $pageNavigation;
    
    use Adianti\Base\AdiantiStandardListTrait;

    public function __construct($param)
    {
        parent::__construct();

        $this->setDatabase('tcc'); // defines the database
        $this->setActiveRecord('Message'); // defines the active record
        $this->setDefaultOrder('id', 'asc');  // defines the default order
        $this->addFilterField('id', '=', 'id'); // add a filter field
        
        TTransaction::open('tcc');

        $this->container = new TVBox;
        $this->container->style = 'width: 100%';
        $this->container->add(TBreadCrumb::create(['Dashboard', 'Dashboard gerencial']));

        $this->form = new BootstrapFormBuilder(self::$formName, 'Filtros');

        $btn_onsearch = $this->form->addAction('Buscar', new TAction([__CLASS__, 'onFilter'], $param), 'fa:search #ffffff');
        $btn_onsearch->addStyleClass('btn-primary');

        $this->makeWindowGeneral();

        $this->container->add($this->form);

        parent::add($this->container);
        TTransaction::close();

        TScript::create("$('#leftsidebar').addClass('menu-hide');
                         $('.action-hide').addClass('close-menu');
                         $('.content').addClass('content-hide');");
    }

    private function makeWindowGeneral()
    {
        $this->form->appendPage('Geral');

        $criteria_class = new TCriteria;
        $criteria_group = new TCriteria;

        $user_id = ApplicationService::getUserID();
        $groups  = ApplicationService::getUserGroupsIDS();
        if(in_array(3, $groups))
        {
            $criteria_class->add(new TFilter('teacher_id', '=', $user_id));
            $criteria_group->add(new TFilter('class_id', 'IN', "(SELECT id FROM class WHERE teacher_id = {$user_id})"));
        }

        $class_id     = new TDBCombo('class_id', 'tcc', 'StudyClass', 'id', '{name}','id asc', $criteria_class);
        $group_id     = new TDBCombo('group_id', 'tcc', 'Group', 'id', '{name}','id asc', $criteria_group);
        $date_process = new TDBCombo('date_process', 'tcc', 'ProcessData', 'date_process', 'date_process','date_process asc');

        $class_id->setChangeAction(new TAction([$this, 'onChangeClass']));
        $group_id->setChangeAction(new TAction([$this, 'onChangeGroup']));

        $class_id->setSize('100%');
        $group_id->setSize('100%');
        $date_process->setSize('100%');

        $row         = $this->form->addFields([new TLabel('Turma*'), $class_id], [new TLabel('Grupo*'), $group_id], [new TLabel('Data processamento*'), $date_process]);
        $row->layout = ['col-sm-4', 'col-sm-4', 'col-sm-4'];

        //$row         = $this->form->addFields([new TLabel('Aluno'), $student_id]);
        //$row->layout = ['col-sm-12'];
    }

    public static function onChangeClass($param)
    {
        try
        {
            if($param['class_id'])
            {
                TTransaction::open('tcc');

                $groups = Group::where('class_id', '=', $param['class_id'])->load();
                
                $array_groups = [];

                if ($groups)
                {
                    foreach ($groups as $group)
                    {
                        $array_groups[$group->id] = $group->name;
                    }
                }

                TDBCombo::reload(self::$formName, 'group_id', $array_groups, TRUE);
                
                TTransaction::close();
            }
        }
        catch(Execption $e)
        {
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }
    }

    public static function onChangeGroup($param)
    {
        try
        {
            if(isset($param['group_id']))
            {
                TTransaction::open('tcc');

                $criteria = new TCriteria;
                $criteria->add(new TFilter('group_id', '=', $param['group_id']));
                $criteria->setProperty('group', 'id, date_process');

                $process_data = ProcessData::getObjects($criteria);
                
                $array_data = [];

                if ($process_data)
                {
                    foreach ($process_data as $data)
                    {
                        $array_data[$data->date_process] = $data->date_process;
                    }
                }

                TDBCombo::reload(self::$formName, 'date_process', $array_data);
                
                TTransaction::close();
            }
        }
        catch(Execption $e)
        {
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }
    }

    public function onFilter($param)
    {
        try
        {
            TTransaction::open('tcc');

            $data = $this->form->getData();

            if(!$data->class_id)
            {
                new TMessage('error', "O campo Turma é obrigatório");
                return;
            }

            if(!$data->group_id)
            {
                new TMessage('error', "O campo Grupo é obrigatório");
                return;
            }

            $this->makeDashboard($data);

            $this->form->setData($data);

            TTransaction::close();
        }
        catch(Execption $e)
        {
            $this->form->setData($data);
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }
    }

    public function makeDashboard($data)
    {
        $html = new THtmlRenderer('app/resources/dashboard.html');
            
        TTransaction::open('tcc');
        $indicator1 = new THtmlRenderer('app/resources/info-box.html');
        $indicator2 = new THtmlRenderer('app/resources/info-box.html');
        $indicator3 = new THtmlRenderer('app/resources/info-box.html');
        $indicator4 = new THtmlRenderer('app/resources/info-box.html');

        $criteria_group    = new TCriteria;
        $criteria_users    = new TCriteria;
        $criteria_messages = new TCriteria;
        $criteria_answer   = new TCriteria;

        if($data->class_id)
        {
            $criteria_group->add(new TFilter('class_id', '=', $data->class_id));
            $criteria_messages->add(new TFilter('group_id', 'IN', "(SELECT groups.id FROM groups WHERE groups.class_id = {$data->class_id})"));
            $criteria_answer->add(new TFilter('message_id', 'IN', "(SELECT message.id FROM message, groups WHERE message.group_id = groups.id AND groups.class_id = {$data->class_id})"));
        }   
        
        if($data->group_id)
        {
            $criteria_group->add(new TFilter('id', '=', $data->group_id));
            $criteria_users->add(new TFilter('group_id', '=', $data->group_id));
            $criteria_messages->add(new TFilter('group_id', '=', $data->group_id));
            $criteria_answer->add(new TFilter('message_id', 'IN', "(SELECT message.id FROM message WHERE message.group_id = {$data->group_id})"));
        }   

        //$criteria_users->add(new TFilter('id', 'IN', "(SELECT DISTINCT system_user_id FROM system_user_group WHERE system_group_id = 2)"));

        $total_groups   = count(Group::getObjects($criteria_group));
        $total_students = count(StudentGroup::getObjects($criteria_users));
        $total_messages = count(Message::getObjects($criteria_messages));
        $total_answer   = count(Answer::getObjects($criteria_answer));
        
        $indicator1->enableSection('main', ['title' => 'Total de mensagens', 'icon' => 'users', 'background' => 'blue', 'value' => ($total_messages+$total_answer)]);
        $indicator2->enableSection('main', ['title' => 'Total de perguntas', 'icon' => 'university', 'background' => 'purple', 'value' => $total_messages]);
        $indicator3->enableSection('main', ['title' => 'Total de respostas', 'icon' => 'code', 'background' => 'green',  'value' => $total_answer]);
        $indicator4->enableSection('main', ['title' => 'Alunos', 'icon' => 'user', 'background' => 'orange', 'value' => $total_students]);

        /*$chart1  = new THtmlRenderer('app/resources/google_pie_chart.html');
        $data1   = [];
        $data1[] = ['Group', 'Users'];
        
        $criteria_messages->setProperty('group', 'id, student_id');
        $stats1 = Message::getObjects($criteria_messages);//groupBy('student_id')->countBy('student_id', 'count');
        if ($stats1)
        {
            foreach ($stats1 as $row)
            {
                $data1[] = [SystemUsers::find($row->student_id)->name, (int)$row->count];
            }
        }
        
        // replace the main section variables
        $chart1->enableSection('main', ['data'   => json_encode($data1),
                                        'width'  => '100%',
                                        'height' => '500px',
                                        'title'  => 'Total de participações por aluno (perguntas e respostas)',
                                        'ytitle' => 'Alunos', 
                                        'xtitle' => _t('Count'),
                                        'uniqid' => uniqid()]);
        
        $chart2 = new THtmlRenderer('app/resources/google_pie_chart.html');
        $data2 = [];
        $data2[] = [ 'Unit', 'Users' ];
        
        //$criteria_answer->setProperty('group', 'id, student_id');
        $stats2 = Answer::getObjects($criteria_answer);//groupBy('student_id')->countBy('student_id', 'count');

        if ($stats2)
        {
            foreach ($stats2 as $row)
            {
                $data2[] = [SystemUsers::find($row->student_id)->name, (int) $row->count];
            }
        }

        // replace the main section variables
        $chart2->enableSection('main', ['data'   => json_encode($data2),
                                        'width'  => '100%',
                                        'height' => '500px',
                                        'title'  => 'Alunos com maior interação',
                                        'ytitle' => 'Alunos', 
                                        'xtitle' => _t('Count'),
                                        'uniqid' => uniqid()]);*/
        
        $html->enableSection('main', ['indicator1' => $indicator1,
                                      'indicator2' => $indicator2,
                                      'indicator3' => $indicator3,
                                      'indicator4' => $indicator4] );


        // creates a Datagrid
        $this->datagrid = new TDataGrid;
        $this->datagrid->disableHtmlConversion();
        $this->datagrid = new BootstrapDatagridWrapper($this->datagrid);
        $this->filter_criteria = new TCriteria;

        $this->datagrid->style = 'width: 100%';
        $this->datagrid->setHeight(320);

        $column_class_id     = new TDataGridColumn('group->class->name', "Turma", 'left');
        $column_activity_id  = new TDataGridColumn('activity->name', "Atividade", 'left');
        $column_group_id     = new TDataGridColumn('group->name', "Grupo", 'left');
        $column_student_name = new TDataGridColumn('student->name', "Aluno", 'left');
        $column_message      = new TDataGridColumn('message', "Mensagem", 'left');
        $column_answers      = new TDataGridColumn('id', "Respostas", 'center');

        $column_answers->setTransformer(['Message', 'getTotalAnswers']);

        $this->datagrid->addColumn($column_class_id);
        $this->datagrid->addColumn($column_activity_id);
        $this->datagrid->addColumn($column_group_id);
        $this->datagrid->addColumn($column_student_name);
        $this->datagrid->addColumn($column_message);
        $this->datagrid->addColumn($column_answers);
        
        // creates the datagrid model
        $this->datagrid->createModel();

        $total_messages = $this->makeReportMessages($criteria_messages);
        
        $panel = new TPanelGroup('Listagem de perguntas');
        $panel->add($this->datagrid)->style = 'overflow-x:auto';
        $panel->addFooter('Total: '.$total_messages);
        
        $dropdown = new TDropDown(_t('Export'), 'fa:list');
        $dropdown->setPullSide('right');
        $dropdown->setButtonClass('btn btn-default waves-effect dropdown-toggle');
        $dropdown->addAction( _t('Save as CSV'), new TAction([$this, 'onExportCSV'], ['register_state' => 'false', 'static'=>'1']), 'fa:table fa-fw blue' );
        $panel->addHeaderWidget( $dropdown );

        $this->datagrid2 = new TDataGrid;
        $this->datagrid2->disableHtmlConversion();
        $this->datagrid2 = new BootstrapDatagridWrapper($this->datagrid2);
        $this->filter_criteria2 = new TCriteria;

        $this->datagrid2->style = 'width: 100%';
        $this->datagrid2->setHeight(320);

        $column_student_name           = new TDataGridColumn('aluno', "Aluno", 'left', '16%');
        $column_total_partipacoes      = new TDataGridColumn('total_participacoes', "Total de participações", 'center', '12%');
        $column_total_partipacoes_sol  = new TDataGridColumn('total_participacoes_solicitas', "Total de participações solícitas", 'center', '12%');
        $column_indice_geral           = new TDataGridColumn('{indice_solicitude}%', "Índice geral de solicitude", 'center', '12%');
        $column_total_answers          = new TDataGridColumn('total_respostas', "Total de respostas", 'center', '12%');
        $column_total_answers_sol      = new TDataGridColumn('total_respostas_solicitas', "Total de respostas solícitas", 'center', '12%');
        $column_indice_geral_respostas = new TDataGridColumn('{indice_solicitude_respostas}%    ', "Índice geral de solicitude nas respostas", 'center', '12%');
        $column_total_interagido       = new TDataGridColumn('0', "Total de perguntas interagidas", 'center', '12%');

        $column_answers->setTransformer(['Message', 'getTotalAnswers']);

        $this->datagrid2->addColumn($column_student_name);
        $this->datagrid2->addColumn($column_total_partipacoes);
        $this->datagrid2->addColumn($column_total_partipacoes_sol);
        $this->datagrid2->addColumn($column_indice_geral);
        $this->datagrid2->addColumn($column_total_answers);
        $this->datagrid2->addColumn($column_total_answers_sol);
        $this->datagrid2->addColumn($column_indice_geral_respostas);
        $this->datagrid2->addColumn($column_total_interagido);
        
        // creates the datagrid model
        $this->datagrid2->createModel();

        $conn    = TTransaction::get();
        $prepare = $conn->prepare(file_get_contents('app/resources/sql/analise_solicitude.sql'));
        $prepare->execute(['group_id'     => $data->group_id,
                           'date_process' => $data->date_process]);
        
        $results = $prepare->fetchAll(PDO::FETCH_OBJ);

        $this->datagrid2->clear();
        if($results)
        {
            // iterate the collection of active records
            foreach ($results as $result)
            {
                // add the object inside the datagrid

                $this->datagrid2->addItem($result);
            }
        }

        $panel2 = new TPanelGroup('Análise de solicitude');
        $panel2->add($this->datagrid2)->style = 'overflow-x:auto';
        //$panel2->addFooter('Total de discussões: '.$total_messages);
        
        $dropdown2 = new TDropDown(_t('Export'), 'fa:list');
        $dropdown2->setPullSide('right');
        $dropdown2->setButtonClass('btn btn-default waves-effect dropdown-toggle');
        $dropdown2->addAction( _t('Save as CSV'), new TAction([$this, 'onExportCSV'], ['register_state' => 'false', 'static'=>'1']), 'fa:table fa-fw blue' );
        $panel2->addHeaderWidget( $dropdown2 );

        $container = new TVBox;
        $container->style = 'width: 100%';
        $container->add($html);
        $container->add($panel2);
        $container->add($panel);
        
        parent::add($container);
        TTransaction::close();
    }

    public function exportAsPDF($param)
    {
        try
        {
            // string with HTML contents
            $html = clone $this->datagrid;
            $contents = file_get_contents('app/resources/styles-print.html') . $html->getContents();
            
            // converts the HTML template into PDF
            $dompdf = new \Dompdf\Dompdf();
            $dompdf->loadHtml($contents);
            $dompdf->setPaper('A4', 'portrait');
            $dompdf->render();
            
            $file = 'app/output/datagrid-export.pdf';
            
            // write and open file
            file_put_contents($file, $dompdf->output());
            
            $window = TWindow::create('Export', 0.8, 0.8);
            $object = new TElement('object');
            $object->data  = $file;
            $object->type  = 'application/pdf';
            $object->style = "width: 100%; height:calc(100% - 10px)";
            $window->add($object);
            $window->show();
        }
        catch (Exception $e)
        {
            new TMessage('error', $e->getMessage());
        }
    }
    
    /**
     * Export datagrid as CSV
     */
    public function exportAsCSV($param)
    {
        try
        {
            // get datagrid raw data
            $data = $this->datagrid->getOutputData();
            
            if ($data)
            {
                $file    = 'app/output/datagrid-export.csv';
                $handler = fopen($file, 'w');
                foreach ($data as $row)
                {
                    fputcsv($handler, $row);
                }
                
                fclose($handler);
                parent::openFile($file);
            }
        }
        catch (Exception $e)
        {
            new TMessage('error', $e->getMessage());
        }
    }
    

    public function makeReportMessages($criteria_messages)
    {
        $repository = new TRepository('Message');
        $limit = 20;

        $criteria = clone $criteria_messages;

        if (empty($param['order']))
        {
            $param['order'] = 'id';    
        }

        if (empty($param['direction']))
        {
            $param['direction'] = 'desc';
        }

        $criteria->setProperties($param); // order, offset
        $criteria->setProperty('limit', $limit);

        if($filters = TSession::getValue(__CLASS__.'_filters'))
        {
            foreach ($filters as $filter) 
            {
                $criteria->add($filter);       
            }
        }

        // load the objects according to criteria
        $objects = $repository->load($criteria, FALSE);

        $this->datagrid->clear();
        if ($objects)
        {
            // iterate the collection of active records
            foreach ($objects as $object)
            {
                // add the object inside the datagrid

                $this->datagrid->addItem($object);
            }
        }

        // reset the criteria for record count
        $criteria->resetProperties();
        $count= $repository->count($criteria);

        return $count;
    }

    public function makeReportProcessData($criteria_messages)
    {
        $repository = new TRepository('ProcessData');
        $limit = 20;

        $criteria = clone $criteria_messages;

        if (empty($param['order']))
        {
            $param['order'] = 'id';    
        }

        if (empty($param['direction']))
        {
            $param['direction'] = 'desc';
        }

        $criteria->setProperties($param); // order, offset
        $criteria->setProperty('limit', $limit);

        if($filters = TSession::getValue(__CLASS__.'_filters'))
        {
            foreach ($filters as $filter) 
            {
                $criteria->add($filter);       
            }
        }

        // load the objects according to criteria
        $objects = $repository->load($criteria, FALSE);

        $this->datagrid2->clear();
        if ($objects)
        {
            // iterate the collection of active records
            foreach ($objects as $object)
            {
                // add the object inside the datagrid

                $this->datagrid2->addItem($object);
            }
        }

        // reset the criteria for record count
        $criteria->resetProperties();
        $count= $repository->count($criteria);

        return $count;
    }
}
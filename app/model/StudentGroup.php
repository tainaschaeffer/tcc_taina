<?php

class StudentGroup extends TRecord
{
    const TABLENAME  = 'student_group';
    const PRIMARYKEY = 'id';
    const IDPOLICY   =  'serial'; // {max, serial}

    private $group;

    /**
     * Constructor method
     */
    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('group_id');
        parent::addAttribute('student_id');
    }

    static public function getStudentsByGroup($group_id)
    {
        return StudentGroup::where('group_id', '=', $group_id)->load();
    }

    /**
     * Method set_group
     * Sample of usage: $var->group = $object;
     * @param $object Instance of group
     */
    public function set_group(Group $object)
    {
        $this->group = $object;
        $this->group_id = $object->id;
    }

    /**
     * Method get_group
     * Sample of usage: $var->group->attribute;
     * @returns group instance
     */
    public function get_group()
    {
    
        // loads the associated object
        if (empty($this->group))
            $this->group = new Group($this->group_id);
    
        // returns the associated object
        return $this->group;
    }   

    public function get_group_name()
    {
    
        // loads the associated object
        if (empty($this->group))
            $this->group = new Group($this->group_id);
    
        // returns the associated object
        return $this->group->name;
    }   
}
<?php

class DiscussionForm extends TPage
{
    protected $form;
    private $formFields = [];
    private static $database = 'tcc';
    private static $activeRecord = 'Message';
    private static $primaryKey = 'id';
    private static $formName = 'form_Message';

    private $fab;
    private $container;
    private $array_answers;
    private $student_id;

    private $rate_icon = 'comments-alt';

    const ENTRY_PARTICIPATE = array('group_id', 'activity_id');

    /**
     * Form constructor
     * @param $param Request
     */
    public function __construct( $param )
    {
        parent::__construct();

        // creates the form
        $this->form = new BootstrapFormBuilder(self::$formName);
        // define the form title
        $this->form->setFormTitle("Discussão");

        $criteriaActivity = new TCriteria;
        $criteriaActivity->add(new TFilter('active', '=', TRUE));

        TTransaction::open('permission');
        $student_id = SystemUsers::newFromLogin(TSession::getValue('login'))->id;   
        TTransaction::close();
        $criteriaGroup = new TCriteria;
        $criteriaGroup->add(new TFilter('id', 'IN', "(SELECT group_id FROM student_group WHERE student_id = {$student_id})"));

        $id          = new THidden('id');
        $message     = new TText('message');
        $group_id    = new TDBCombo('group_id', 'tcc', 'Group', 'id', '{name} - {class->name}','id asc', $criteriaGroup);
        $activity_id = new TDBCombo('activity_id', 'tcc', 'Activity', 'id', '{name} - {class->name}','id desc', $criteriaActivity);

        $message->addValidation("Mensagem", new TRequiredValidator());
        $group_id->addValidation("Grupo", new TRequiredValidator());
        $activity_id->addValidation("Atividade", new TRequiredValidator());

        $message->setSize('100%', 100);
        $group_id->setSize('100%');
        $activity_id->setSize('100%');

        $group_id->setDefaultOption(FALSE);
        $activity_id->setDefaultOption(FALSE);

        $row        = $this->form->addFields([new TLabel("Grupo:"), $group_id], [new TLabel("Atividade:"), $activity_id]);
        $row->style = ['col-sm-6', 'col-sm-6'];
        $row->style = 'display: none;';
        $row->class = 'participate';

        $row        = $this->form->addFields([new TLabel("Mensagem:"), $message, $id]);
        $row->style = ['col-sm-12'];

        // create the form actions
        $btn_onsave = $this->form->addAction("Salvar", new TAction([$this, 'onSave']), 'fas:save #ffffff');
        $btn_onsave->addStyleClass('btn-primary'); 

        $btn_onclear = $this->form->addAction("Limpar formulário", new TAction([$this, 'onClear']), 'fas:eraser #dd5a43');

        $btn_onback = $this->form->addAction("Voltar para discussões", new TAction(['DiscussionList', 'onShow']), 'fas:angle-left #dd5a43');

        // vertical box container
        $this->container = new TVBox;
        $this->container->style = 'width: 100%';
        $this->container->class = 'form-container';
        $this->container->add($this->form);

        parent::add($this->container);
    }

    public function onSave($param = null) 
    {
        try
        {
            TTransaction::open(self::$database); // open a transaction
            $messageAction = null;

            $this->form->validate(); // validate form data

            $object = new Message(); // create an empty object 

            $student_id = SystemUsers::newFromLogin(TSession::getValue('login'))->id;

            $data               = $this->form->getData(); // get form data as array

            $data->date_message = date('Y-m-d H:i:s');
            $data->student_id   = $student_id;

            $object->fromArray( (array) $data); // load the object with data

            $object->store(); // save the object 

            // get the generated {PRIMARY_KEY}
            $data->id = $object->id; 

            $this->form->setData($data); // fill form data

            $this->makeTimeline($data);
            $this->makeFab($data);
            
            TTransaction::close(); // close the transaction

            new TMessage('info', "Registro salvo", $messageAction); 
        }
        catch (Exception $e) // in case of exception
        {
            new TMessage('error', $e->getMessage()); // shows the exception error message
            $this->form->setData( $this->form->getData() ); // keep form data
            TTransaction::rollback(); // undo all pending operations
        }
    }

    public function onEdit( $param )
    {
        try
        {
            if (isset($param['key']))
            {
                self::showEntry(TRUE);

                TScript::create("$('.card-header').show();
                                 $('.participate').show();
                                 $('#tbutton_btn_limpar_formulário').show();
                                 $('#tbutton_btn_salvar').show();");

                $key = $param['key'];  // get the parameter $key

                TTransaction::open(self::$database); // open a transaction

                $object = new Message($key); // instantiates the Active Record 

                $this->form->setData($object); // fill the form 

                $this->makeTimeline($object);
                $this->makeFab($object);

                TTransaction::close(); // close the transaction 
            }
            else
            {
                $this->form->clear();

                TScript::create("$('.card-header').show();
                                 $('.participate').show();
                                 $('#tbutton_btn_limpar_formulário').show();
                                 $('#tbutton_btn_salvar').show();");
            }
        }
        catch (Exception $e) // in case of exception
        {
            new TMessage('error', $e->getMessage()); // shows the exception error message
            TTransaction::rollback(); // undo all pending operations
        }
    }

    private static function showEntry($fl_show = FALSE)
    {
        if(self::ENTRY_PARTICIPATE)
        {
            foreach(self::ENTRY_PARTICIPATE as $entry)
            {
                if($fl_show)
                {
                    BootstrapFormBuilder::showField(self::$formName, $entry);
                }
                else
                {
                    BootstrapFormBuilder::hideField(self::$formName, $entry);
                }
            }
        }
    }

    public function onParticipate( $param )
    {
        try
        {
            if (isset($param['key']))
            {
                self::showEntry(FALSE);
                TScript::create("$('.card-header').hide();
                                 $('#tbutton_btn_limpar_formulário').hide();
                                 $('#tbutton_btn_salvar').hide();");

                $key = $param['key'];  // get the parameter $key

                TTransaction::open(self::$database); // open a transaction

                $object = new Message($key); // instantiates the Active Record 

                $this->form->setData($object); // fill the form 

                $this->makeTimeline($object);
                $this->makeFab($object);

                TTransaction::close(); // close the transaction 
            }
            else
            {
                $this->form->clear();
            }
        }
        catch (Exception $e) // in case of exception
        {
            new TMessage('error', $e->getMessage()); // shows the exception error message
            TTransaction::rollback(); // undo all pending operations
        }
    }

    public function makeFab($dados)
    {
        $action = new TAction(['AnswerForm', 'onLoad'], ['message_id' => $dados->id,
                                                         'student_id' => $this->student_id]);

        $this->fab = new TFabButton('my_fab','#29B6F6','fas:plus #fff', 'Ações');
        $this->fab->addItem('Enviar resposta', $action, 'fas:comments', '#FFF');
        $this->container->add($this->fab);
    }

    public function makeTimeline($message)
    {
        try
        {
            TTransaction::open(self::$database); // open a transaction

            $criteria = new TCriteria;
            $criteria->add(new TFilter('message_id', '=', $message->id));
            $criteria->setProperty('order', 'date_answer desc');

            $answers = Answer::getObjects($criteria); // instantiates the Active Record

            TTransaction::close(); // close the transaction 

            $this->student_id = SystemUsers::newFromLogin(TSession::getValue('login'))->id;

            TTransaction::open('permission');
                
            $user = new SystemUsers($this->student_id);

            $groups = $user->getSystemUserGroupIds();
            $display_rate = FALSE;
            if(in_array(SystemGroup::TEACHER, $groups))
            {
                $display_rate = TRUE;
            }

            TTransaction::close();

            $this->array_answers = [];
            if($answers)
            {
                $timeline = new TTimeline;

                foreach ($answers as $answer) 
                {
                    if($answer->student_id == $message->student_id)
                    {
                        $icon     = 'fa:arrow-left bg-green';
                        $position = 'left';
                    }
                    else
                    {
                        $icon     = 'fa:arrow-right bg-blue';
                        $position = 'right';
                    }

                    if($answer->student_id == $this->student_id)
                    {
                        $this->array_answers[$answer->id] = $answer->id;
                    }

                    $student_answer = $answer->answer;

                    if($answer->attachment)
                    {
                        $button               = new TElement('a');
                        $button->{'href'}     = "download.php?file=files/answer/$answer->id/$answer->attachment";
                        $button->{'generate'} = "adianti";
                        $button->{'class'}    = 'btn btn-default';
                        $button->add('Ver anexo');

                        $student_answer .= "<br><br> {$button} <br>";
                    }


                    if($answer->rating)
                    {
                        $key_rating  = array_search($answer->rating, Answer::RATING);

                        if(array_key_exists($key_rating+1, Answer::RATING_ICON))
                        {
                            $key_rating = $key_rating+1;
                        }
                        else
                        {
                            $key_rating = 1;
                        }

                        $this->rate_icon = Answer::RATING_ICON[$key_rating];
                    }
                    else
                    {
                        $this->rate_icon = Answer::RATING_ICON[1];
                    }

                    $i                = new TElement('i');
                    $i->{'class'}     = "fas fa-{$this->rate_icon} #555";
                    $i->{'style'}    = "";

                    $btn              = new TElement('a');

                    if($display_rate)
                    {
                        $btn->{'onclick'} = "__adianti_load_page('index.php?class=DiscussionForm&method=onRateAnswer&key={$answer->id}');";
                    }

                    $btn->{'class'}   = 'btn btn-default btn-sm';
                    $btn->add($i);

                    $student_answer .= " <br> $btn";

                    $timeline->addItem($answer->id, "{$answer->student_name}",  "{$student_answer}", "{$answer->date_answer}",  $icon,  $position,  $answer);
                }

                $display_condition = function( $object = false ) 
                {
                    if(array_key_exists($object->id, $this->array_answers))
                    {
                        return true;
                    }

                    return false;
                };
                
                $action_onEditAnswer   = new TAction(['AnswerForm', 'onEdit'], ['key' => '{id}']);
                $action_onDeleteAnswer = new TAction([$this, 'onDeleteAnswer'], ['key' => '{id}']);
                $action_onRateAnswer   = new TAction([$this, 'onRateAnswer'], ['key' => '{id}']);
                
                $action_onEditAnswer->setProperty('btn-class', 'btn btn-primary');
                $action_onDeleteAnswer->setProperty('btn-class', 'btn btn-danger');
                $action_onRateAnswer->setProperty('btn-class', 'btn');
                
                $timeline->addAction($action_onEditAnswer, '', 'far:edit #fff', $display_condition);
                $timeline->addAction($action_onDeleteAnswer, '', 'fas:trash-alt #fff', $display_condition);

                $timeline->setUseBothSides();
                $timeline->setTimeDisplayMask('dd/mm/yyyy');
                $timeline->setFinalIcon( 'fa:genderless bg-red' );

                parent::add($timeline);
            }
        }
        catch (Exception $e) // in case of exception
        {
            new TMessage('error', $e->getMessage()); // shows the exception error message
            TTransaction::rollback(); // undo all pending operations
        }
    }

    public function onRateAnswer($param)
    {
        try
        {
            if (isset($param['key']))
            {
                $key = $param['key'];  // get the parameter $key

                TTransaction::open(self::$database); // open a transaction

                $object = new Answer($key); // instantiates the Active Record 

                if($object->rating)
                {
                    $key_rating  = array_search($object->rating, Answer::RATING);

                    if(array_key_exists($key_rating+1, Answer::RATING))
                    {
                        $key_rating = $key_rating+1;
                    }
                    else
                    {
                        $key_rating = 1;
                    }

                    $next_rating = Answer::RATING[$key_rating];
                    $next_rating = explode(' ', $next_rating);

                    $object->rating = $next_rating[0];

                    $this->rate_icon = Answer::RATING_ICON[$key_rating];
                }
                else
                {
                    $rating = Answer::RATING[1];
                    $rating = explode(' ', $rating);

                    $object->rating = $rating[0];

                    $this->rate_icon = Answer::RATING_ICON[1];
                }

                $object->store();

                $this->onParticipate(['key' => $object->message_id]);

                TTransaction::close(); // close the transaction 
            }
            else
            {
                $this->form->clear();
            }
        }
        catch (Exception $e) // in case of exception
        {
            new TMessage('error', $e->getMessage()); // shows the exception error message
            TTransaction::rollback(); // undo all pending operations
        }
    }

    public function onDeleteAnswer($param = NULL)
    {
        if(isset($param['delete']) && $param['delete'] == 1)
        {
            try
            {
                // get the paramseter $key
                $key = $param['key'];
                // open a transaction with database
                TTransaction::open(self::$database);

                // instantiates object
                $object = new Answer($key, FALSE); 

                $message_id = $object->message_id;

                // deletes the object from the database
                $object->delete();

                // close the transaction
                TTransaction::close();

                $this->onParticipate(['key' => $message_id]);

                // shows the success message
                new TMessage('info', AdiantiCoreTranslator::translate('Record deleted'));
            }
            catch (Exception $e) // in case of exception
            {
                // shows the exception error message
                new TMessage('error', $e->getMessage());
                // undo all pending operations
                TTransaction::rollback();
            }
        }
        else
        {
            // define the delete action
            $action = new TAction(array($this, 'onDeleteAnswer'));
            $action->setParameters($param); // pass the key paramseter ahead
            $action->setParameter('delete', 1);
            // shows a dialog to the user
            new TQuestion(AdiantiCoreTranslator::translate('Do you really want to delete ?'), $action);   
        }
        
    }

    /**
     * Clear form data
     * @param $param Request
     */
    public function onClear( $param )
    {
        $this->form->clear(true);

        TScript::create("$('.card-header').show();
                                 $('.participate').show();
                                 $('#tbutton_btn_limpar_formulário').show();
                                 $('#tbutton_btn_salvar').show();");
    }

    public function onShow($param = null)
    {
    } 

}


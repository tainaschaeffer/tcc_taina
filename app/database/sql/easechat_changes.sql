-- 2020-09-04
ALTER TABLE class ADD COLUMN teacher_id INTEGER NOT NULL;

ALTER TABLE message DROP COLUMN subject;

ALTER TABLE answer ADD COLUMN rating VARCHAR(20);

CREATE TABLE preference(
	id SERIAL NOT NULL, 
	user_id INT NOT NULL, 
	fl_filter_hidden BOOLEAN NOT NULL DEFAULT FALSE, 
	fl_menu_hiddenn BOOLEAN NOT NULL DEFAULT FALSE,
	PRIMARY KEY(id));

-- 2020-09-11
ALTER TABLE answer ADD COLUMN attachment TEXT;

--2020-09-15
ALTER TABLE activity ADD COLUMN active BOOLEAN;

--2020-09-25
ALTER TABLE message add column group_id integer references groups(id);

--2020-10-25
CREATE TABLE process_data(
	id SERIAL NOT NULL, 
	type VARCHAR(1) NOT NULL, 
	description TEXT NOT NULL, 
	description_id INT NOT NULL,
	date_process TIMESTAMP NOT NULL,
	prob_0 TEXT NOT NULL,
	prob_1 TEXT NOT NULL,
	student_id INT NOT NULL,
	PRIMARY KEY(id));

ALTER TABLE process_data ADD COLUMN group_id integer references groups(id);